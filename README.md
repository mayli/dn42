# ansible playbook for dn42 routers

# deploy

`ansible-playbook --vault-password-file .vault-pass.txt -i production site.yml`

# add peer
 - add peer in host_vars/<host>/peers.yml
 - add secret key in host_vars/<host>/<peer_name>.yml
   using `ansible-vault  --vault-password-file .vault-pass.txt encrypt_string >> host_vars/<host>/<peer>.yml`

# check

`ansible --vault-password-file .vault-pass.txt -i production -a "birdc show p" all`
